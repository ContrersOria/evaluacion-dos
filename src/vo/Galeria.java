/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vo;

import java.util.Scanner;

/**
 *
 * @author oacon
 */
public class Galeria {

    private String ubicacionGaleria;
    private String nombreGaleria;

    public Galeria(String ubicacionGaleria, String nombreGaleria) {
        this.ubicacionGaleria = ubicacionGaleria;
        this.nombreGaleria = nombreGaleria;
    }

    public Galeria() {

    }

    public String getUbicacionGaleria() {
        return ubicacionGaleria;
    }

    public void setUbicacionGaleria(String ubicacionGaleria) {
        this.ubicacionGaleria = ubicacionGaleria;
    }

    public String getNombreGaleria() {
        return nombreGaleria;
    }

    public void setNombreGaleria(String nombreGaleria) {
        this.nombreGaleria = nombreGaleria;
    }

    public void crearGaleria(String nombre, String ubicacion) {
        System.out.println("***** Crear Galeria *****");
        this.nombreGaleria = nombre;
        this.ubicacionGaleria = ubicacion;
        System.out.println("Galeria " + nombre + " creada ");
    }

    public void actualizarGaleria(String nombre, String ubicacion) {
        System.out.println("***** Modificar Galeria *****");
        System.out.println("Galeria actual: " + this.nombreGaleria + " " + " ubicada en " + ubicacionGaleria);
        this.nombreGaleria = nombre;
        this.ubicacionGaleria = ubicacion;
        System.out.println("Galeria modificada: " + this.nombreGaleria + " " + " ubicada en " + ubicacionGaleria);

    }

    public void eliminarGaleria() {
        Scanner sc = new Scanner(System.in);
        String decision;

        System.out.println("***** Eliminar Galeria *****");
        System.out.println("¿Está usted seguro de eliminar la Galeria " + this.nombreGaleria + " ubicada en " + this.ubicacionGaleria + " ? S/N");
        decision = sc.next();
        
        this.nombreGaleria = null;
        this.ubicacionGaleria = null;
       

        System.out.println("+* Galeria eliminada correctamente **");

    }

    public void mostrarGaleria() {
        System.out.println("***** -----Mostrando Galeria---- *****");
        System.out.println("Galeria: " + this.nombreGaleria + " " + " ubicada en " + this.ubicacionGaleria);
    }

}
