
import java.util.Scanner;
import vo.Administrador;
import vo.Galeria;
import vo.Usuario;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author oacon
 */
public class ControladorPrincipal {

    /**
     * @param args the command line arguments
     */
    public static Galeria galeria1;
    public static Galeria galeria2;
    public static Galeria galeria3;
    public static String nombre;
    public static String ubicacion;
    public static Menu objMenu;

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int seleccion = 0;
        int seleccionGaleria = 0;
        String userLogin = "";
        boolean isUserValid = false;
        boolean isAdministrator = false;

        System.out.println("¿Desea usted ingresar como administrador = 1 o Usuario = 2 ? ");

        int respuesta = sc.nextInt();

        if (respuesta == 1) {
            // isAdministrator = true;

            do {
                Administrador ob1 = new Administrador();
                String user = ob1.email();
                String pass = ob1.clave();
                userLogin = ob1.Login(user, pass);
                isAdministrator = (userLogin.length() == 0);
            } while (isAdministrator);

        } else {
            //isAdministrator = false; 

            do {
                Usuario ob = new Usuario();
                String user = ob.usuario();
                String pass = ob.pass();
                userLogin = ob.Login(user, pass);
                isUserValid = (userLogin.length() == 0);
            } while (isUserValid);
            System.exit(0);
        }

        do {

            objMenu = new Menu();

            objMenu.ejecutarMenu();
            seleccion = objMenu.getOp();

            System.out.println("Seleccion " + seleccion);

            seleccionGaleria = objMenu.getOpcionGaleriaSeleccionada();
            System.out.println("Seleccion Galeria " + seleccionGaleria);

            nombre = objMenu.getNombreGaleria();
            ubicacion = objMenu.getUbicacionGaleria();

            seleccionGaleria = objMenu.getOpcionGaleriaSeleccionada();
            decisionUsuario(seleccion, seleccionGaleria, ubicacion, nombre);
        } while (seleccion != 5);

    }//fin main

    public static void decisionUsuario(int seleccion, int seleccionGaleria, String ubicacion, String nombre) {
        switch (seleccion) {

            case 1:// cuando se elige creacion
                switch (seleccionGaleria) {
                    case 1:
                        galeria1 = new Galeria();
                        galeria1.crearGaleria(objMenu.getNombreGaleria(), objMenu.getUbicacionGaleria());

                        break;

                    case 2:
                        galeria2 = new Galeria();
                        galeria2.crearGaleria(objMenu.getNombreGaleria(), objMenu.getUbicacionGaleria());

                        break;

                    case 3:
                        galeria3 = new Galeria();
                        galeria3.crearGaleria(objMenu.getNombreGaleria(), objMenu.getUbicacionGaleria());

                        break;

                    default:

                        throw new AssertionError();
                }
                break;

            case 2://cuando se elige Actualizar
                switch (seleccionGaleria) {
                    case 1:
                        galeria1.actualizarGaleria(nombre, ubicacion);
                        break;
                    case 2:
                        galeria2.actualizarGaleria(nombre, ubicacion);
                        break;
                    case 3:
                        galeria3.actualizarGaleria(nombre, ubicacion);
                        break;
                    default:
                        break;
                }
                break;

            case 3://cuando se elige eliminar 
                switch (seleccionGaleria) {
                    case 1:
                        galeria1.eliminarGaleria();
                        break;
                    case 2:
                        galeria2.eliminarGaleria();
                        break;
                    case 3:
                        galeria3.eliminarGaleria();
                        break;
                    default:
                        break;
                }
                break;

            case 4:// cuando se elige mostrar
                switch (seleccionGaleria) {
                    case 1:
                        if (galeria1 != null) {
                            
                            if (galeria1.getNombreGaleria() == null && galeria1.getUbicacionGaleria() == null) {

                                System.out.println(" No se encuentra creada una Galeria 1 ");

                            } else {
                                galeria1.mostrarGaleria();
                            }

                        } else {
                            System.out.println("Error!!!! Usted aun no ha creado la galeria 1");
                        }
                        break;
                    case 2:
                        if (galeria2 != null) {
                            if (galeria2.getNombreGaleria() == null && galeria2.getUbicacionGaleria() == null) {

                                System.out.println(" No se encuentra creada una Galeria 2 ");

                            } else {
                                galeria2.mostrarGaleria();
                            }
                            
                        } else {
                            System.out.println("Error!!!! Usted aun no ha creado la galeria 2");
                        }
                    case 3:
                        if (galeria3 != null) {
                            if (galeria3.getNombreGaleria() == null && galeria3.getUbicacionGaleria() == null) {

                                System.out.println(" No se encuentra creada una Galeria 3 ");

                            } else {
                                galeria3.mostrarGaleria();
                            }
                            
                        } else {
                            System.out.println("Error!!!! Usted aun no ha creado la galeria 3");
                        }
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }

    }

}//fin class
