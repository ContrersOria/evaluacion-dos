
import java.util.Scanner;
import vo.Galeria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author oacon
 */
public class Menu {

    private int opcionSeleccionada;
    private int opcionGaleriaSeleccionada;
    private String nombreGaleria;
    private String ubicacionGaleria;
    public Scanner scanner;

    Galeria objGaleria = new Galeria();

    public int getOp() {
        return opcionSeleccionada;
    }

    public void setOp(int opcion) {
        this.opcionSeleccionada = opcion;
    }

    public void Menu() {

    }

    public void ejecutarMenu() {
        int op = 0;

        do {
            boolean opcionEsValida = true;
            do {
                this.mostrarMenu();
                //op = scanner.nextInt();
                scanner = new Scanner(System.in);
                String opcionUsuario = scanner.nextLine();
                //validar que la opci{on ingresada sea un n{umoer

                opcionEsValida = Menu.isNumeric(opcionUsuario);
                if (!opcionEsValida) {
                    System.out.println("OPCIÓN NO VALIDA");
                    continue;
                }
                op = Integer.parseInt(opcionUsuario);

                opcionEsValida = (op > 0 && op < 6);
                if (!opcionEsValida) {
                    System.out.println("OPCIÓN NO VALIDA");
                }
            } while (!opcionEsValida);

            switch (op) {
                case 1:
                    System.out.println("opcion 1");
                    this.opcionSeleccionada = op;
                    this.mostrarMenuGaleria();
                    return;

                case 2:
                    System.out.println("opcion 2");
                    this.opcionSeleccionada = op;
                    this.mostrarMenuGaleria();
                    return;

                case 3:
                    System.out.println("opcion 3");
                    this.opcionSeleccionada = op;
                    this.mostrarMenuGaleria();

                    return;

                case 4:
                    System.out.println("opcion 4");
                    this.opcionSeleccionada = op;
                    this.mostrarMenuGaleria();
                    return;

                case 5:
                    System.out.println("opcion 5");
                    this.opcionSeleccionada = op;
                    return;

                default:

                    throw new AssertionError();
            }
        } while (op != 6);

    }

    public void mostrarMenu() {

        System.out.println("");
        System.out.println("---------------------------");
        System.out.println("|      Menu Principal     |");
        System.out.println("|  1.- Crear Galeria      |");
        System.out.println("|  2.- Actualizar Galeria |");
        System.out.println("|  3.- Eliminar Galeria   |");
        System.out.println("|  4.- Mostrar Galeria    |");
        System.out.println("|  5.- Salir              |");
        System.out.println("---------------------------");
        System.out.print("      Opcion: ");
    }

    public void mostrarMenuGaleria() {
        System.out.println("Por favor seleccione la galeria a tratar, puede elegir entre opciones 1,2,3. ");
        int op = 0;

        //op = scanner.nextInt();
        op = Integer.parseInt(scanner.nextLine());

        switch (this.opcionSeleccionada) {
            case 1:
                System.out.println("------------Crear Galerias---------------");
                this.opcionGaleriaSeleccionada = op;
                System.out.println("Ingrese nombre de la nueva galeria: " + op);
                this.setNombreGaleria(scanner.nextLine());
                System.out.println("Ingrese ubicación de la nueva galeria: " + op);
                this.setUbicacionGaleria(scanner.next());
                return;

            case 2:
                System.out.println("------------Actualizar de Galerias---------------");
                this.opcionGaleriaSeleccionada = op;
                System.out.println("Ingrese nombre de la nueva galeria: " + op);
                this.setNombreGaleria(scanner.nextLine());
                System.out.println("Ingrese ubicación de la nueva galeria: " + op);
                this.setUbicacionGaleria(scanner.next());
                return;

            case 3:
                System.out.println("------------Eliminar de Galerias---------------");
                System.out.println("Seleccione Galeria a crear 1,2 o 3: ");
                this.opcionGaleriaSeleccionada = op;
                
                
                return;
            //break;

            case 4:
                System.out.println("------------Mostrar de Galerias---------------");
                System.out.println("Seleccione Galeria a crear 1,2 o 3: ");
                this.opcionGaleriaSeleccionada = op;
                
                return;
            //break;

        }

    }

    /**
     * @return the opcionGaleriaSeleccionada
     */
    public int getOpcionGaleriaSeleccionada() {
        return opcionGaleriaSeleccionada;
    }

    /**
     * @param opcionGaleriaSeleccionada the opcionGaleriaSeleccionada to set
     */
    public void setOpcionGaleriaSeleccionada(int opcionGaleriaSeleccionada) {
        this.opcionGaleriaSeleccionada = opcionGaleriaSeleccionada;
    }

    /**
     * @return the nombreGaleria
     */
    public String getNombreGaleria() {
        return nombreGaleria;
    }

    /**
     * @param nombreGaleria the nombreGaleria to set
     */
    public void setNombreGaleria(String nombreGaleria) {
        this.nombreGaleria = nombreGaleria;
    }

    /**
     * @return the ubicacionGaleria
     */
    public String getUbicacionGaleria() {
        return ubicacionGaleria;
    }

    /**
     * @param ubicacionGaleria the ubicacionGaleria to set
     */
    public void setUbicacionGaleria(String ubicacionGaleria) {
        this.ubicacionGaleria = ubicacionGaleria;
    }

    public static boolean isNumeric(String str) {
        try {
            int number = Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}// fin class Menu
